begin_version
3
end_version
begin_metric
1
end_metric
2
begin_variable
var0
-1
2
Atom at-goal(stone-03)(false)
Atom at-goal(stone-03)(true)
end_variable
begin_variable
var1
-1
2
Atom at(player-01, pos-5-3)(false)
Atom at(player-01, pos-5-3)(true)
end_variable
0
begin_state
0
0
end_state
begin_goal
1
0 1
end_goal
2
begin_operator
(move player-01 pos-4-3 pos-5-3 dir-right)_1
0
1
0 1 1 1
0
end_operator
begin_operator
(push-to-nongoal player-01 stone-03 pos-6-3 pos-5-3 pos-4-3 dir-left)_2
0
1
0 1 0 1
1
end_operator
0
