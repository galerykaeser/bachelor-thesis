\chapter{Conclusions}\label{ch:conclusion}
The goal of this thesis project was to implement a planning task minimizer for finding small instances of PDDL or $\text{SAS}^+$ tasks causing some unexpected behavior on a planning system.
The search for smaller task instances in the implemented minimizer is realized by successive random selection and removal of a task element of one of a few specific types, which are \textit{actions}, \textit{objects} and \textit{predicates} for PDDL tasks and \textit{operators} and \textit{variables} for $\text{SAS}^+$ tasks.
Task elements are removed in a syntactically consistent way from every occurrence in the task.
However, no semantic integrity of tasks is guaranteed after this minimization process.
%All experiments were performed with the hard-coded integer constant 42 as the seed for randomization of task elements before the selection of the next candidate for removal.

\section{Evaluation of Use-Cases}\label{sec:use-cases}
We illustrate the functionality of the minimizer in Chapter~\ref{ch:use-cases} on the basis of three use-cases featuring planning tasks causing incorrect or unexpected results on the Fast Downward Planning System.
In Section~\ref{sec:use-case-1:-issue-335}, we introduce the first tested use-case, which initially is made up of, among others, 46 actions, 62 objects and 29 predicate symbols.
According to the measurements in Table~\ref{tab:avg-results-issue335}, the overall best result in terms of the number of remaining task elements is achieved with delete option order \textit{action, predicate, object}, shrinking the task to 2 actions, 6 objects and 4 predicates.
In use-case 2, we run the minimizer on a $\text{SAS}^+$ planning problem initially containing, among others, 1536 operators and 184 variables.
From the results presented in Table~\ref{tab:avg-results-seg-fault}, we can see that, in terms of the greatest reduction of task elements, delete option order \textit{variable, operator} does the best job.
Runs with this configuration reduce the amount of operators as well as the amount of variables to 2 in the remaining task.
In use-case 3, the best runs (by the same metric as above) are the two whose delete options start with \textit{predicate}, followed by
\textit{object} and \textit{action} in arbitrary order (Table~\ref{tab:avg-results-issue736}).
In these runs, the PDDL task initially containing, among others, 3 actions, 5 objects and 6 predicates is reduced to 1 action, 3 objects and 3 predicates.
There are two additional remarks we want to make regarding the results of the use-cases:
\begin{enumerate}
    \item The current version of the minimizer is not able to determine an optimal order of delete options by itself.
    The \textit{best order} of options, as we called it above, solely represents the apparent best order for a specific use-case, compared to test runs with different configurations.
    Not all possible configurations were tested.
    \item We choose not to compare the average runtimes of different runs in this thesis, because we want to keep the focus on the minimization results.
    We do believe there are improvements that could be made to the minimizer that may decrease search time or improve task minimization or both.
    We discuss ideas for future work in Section~\ref{sec:future-work}.
\end{enumerate}

\section{Future Work}\label{sec:future-work}
There are multiple ways the task minimizer could be extended to improve its functionality.
Currently, the search is rather uninformed, as it uses first-choice hill-climbing.
Some kind of meta-heuristic could be used to estimate the size of successor tasks, so the smallest one could be picked each iteration.
An idea for this meta-heuristic would be to count, for example, which object appears most times in the current task.
This object would be the next one to be removed from the task by the transformer.
Such a meta-heuristic could also eliminate the need for the user to specify which task element should be deleted -- the minimizer could choose the best option each iteration by itself.
Implementing additional delete options like axioms or mutexes or finding other ways to parameterize the search space could also be useful extensions.
Currently, planner runs have to terminate before the output can be parsed.
This is a waste of time if the user is interested in some planner output that is available early on.
In the scope of this thesis, we have not investigated whether there are ways to parse generated output on the fly.
If this can be achieved, the execution could be terminated prematurely, as soon as the characteristic is found.
Finally, re-implementing the minimizer in a compiled language such as C++ could improve performance significantly as well.