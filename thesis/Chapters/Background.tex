\chapter{Background}\label{ch:background}
In this chapter, we introduce relevant concepts for this thesis project.
We provide a brief description of Automated Planning and a definition of the PDDL and FDR ($\text{SAS}^+$) tasks that are accepted by the Fast Downward Planning System~\cite{helmert2006fast}, which the project is based upon.
Furthermore, we describe the hill-climbing algorithm~\cite{russellartificial}, which is used at the core of the minimizer.

\section{Automated Planning}\label{sec:automated-planning}
Automated Planning focuses on developing algorithms and heuristics for automatic solving of planning tasks.
Planning tasks are state space search problems and can be modelled by means of an input language.
The input language defines properties of the problem, such as an initial state, a goal and actions that lead from one state to another.
The objective of planning is to find a valid action sequence, i.e., a \textit{plan}, leading from the initial state to the goal.


\section{PDDL and FDR ($\text{SAS}^+$) Tasks}\label{sec:pddl-tasks}
The task minimizer implemented for this thesis uses Python modules that were developed for the translator of the Fast Downward Planning System.
This means, the PDDL variant of our minimizer accepts the same kind of PDDL tasks Fast Downward does, which is ``the \textit{non-numerical}, \textit{non-temporal} fragment of PDDL 2.2''~\cite{helmert2009concise}.
In this section, we provide formal definitions for such a planning task.
The following four definitions are from \citeauthor{helmert2009concise}~\citeyearpar{helmert2009concise} with a small modification: wherever the term \textit{operator} was used in the context of PDDL in the original paper, we use the term \textit{action}.
\begin{definition}[PDDL actions]
    \label{def:pddl_op}
    A \textbf{PDDL action} is a pair $\langle\chiup,e\rangle$, which consists of a (possibly open) first-order formula $\chiup$ called its \textbf{precondition} and a \textbf{PDDL effect} e. PDDL effects are recursively defined by finite application of the following rules:
    \begin{itemize}
        \item A first-order literal $l$ is a PDDL effect called a \textbf{simple effect}.
        \item If $e_1,\dots,e_n$ are PDDL effects, then $e_1\wedge\dots\wedge e_n$ is a PDDL effect called a \textbf{conjunctive effect}.
        \item If $\chiup$ is a first-order formula and e is a PDDL effect, then $\chiup\rhd e$ is a PDDL effect called a \textbf{conditional effect}.
        \item If $v_1,\dots,v_k$ are variable symbols and $e$ is a PDDL effect, then $\forall v_1\dots v_k:e$ is a PDDL effect called a \textbf{universally quantified effect} or \textbf{universal effect}.
    \end{itemize}
    Free variables of simple effects are defined as for literals in first-order logic.
    Free variables of other effects are defined by structural induction:
    \begin{itemize}
        \item $\textup{free}(e_1\wedge\dots\wedge e_n)=\textup{free}(e_1)\cup \dots\cup\textup{free}(e_n)$
        \item $\textup{free}(\chiup\rhd e)=\textup{free}(\chiup)\cup\textup{free}(e)$
        \item $\textup{free}(\forall v_1\dots v_k:e)=\textup{free}(e)\backslash\{v_1,\dots,v_k\}$
    \end{itemize}
    The set of free variables of a PDDL action is defined as $\textup{free}(\langle\chiup,e\rangle)=\textup{free}(\chiup)\cup\textup{free}(e)$. Free variables are also called \textbf{parameters} of the action.
\end{definition}
%The terms \emph{action} and \emph{operator} are treated as synonyms in this paper. Since the translator implemented in Fast Downward uses the term \emph{action}, this is the term which will be used for the remainder of this paper (exceptions are cited definitions which use the term \textit{operator}).

\begin{definition}[PDDL axioms]
    A \textbf{PDDL axiom} is a pair $\langle\varphi,\psi\rangle$ such that $\varphi$ is a first-order atom and $\psi$ is a first-order formula with $\textup{free}(\psi)\subseteq\textup{free}(\varphi)$. We write the axiom $\langle\varphi,\psi\rangle$ as $\varphi\leftarrow\psi$ and call $\varphi$ the \textbf{head} and $\psi$ the \textbf{body} of the axiom.\\
    \\
    A set $\mathcal{A}$ of PDDL axioms is called \textbf{stratifiable} iff there exists a total preorder $\preceq$ on the predicate symbols of $\mathcal{A}$ such that for each axiom where predicate $Q$ occurs in the head, we have $P\preceq Q$ for all predicates $P$ occurring in the body, and $P\prec Q$ for all predicates $P$ occurring in a negative literal in the translation of the body to negation normal form.
\end{definition}
\begin{definition}[PDDL tasks]
    A \textbf{PDDL task} is given by a 4-tuple $\Pi=\langle\chiup_0,\chiup_\star,\mathcal{A},\mathcal{O}\rangle$ with the following components:
    \begin{itemize}
        \item $\chiup_0$ is a finite set of ground atoms called the \textbf{initial state}.
        \item $\chiup_\star$ is a closed formula called the \textbf{goal formula}.
        \item $\mathcal{A}$ is a finite stratified set of PDDL axioms.
        \item $\mathcal{O}$ is a finite set of PDDL actions.
    \end{itemize}
    Predicates occurring in the head of an axiom in $\mathcal{A}$ are called \textbf{derived predicates}.
    Predicates occurring in the initial state or in simple effects of actions in $\mathcal{O}$ are called \textbf{fluent predicates}.
    The sets of derived and fluent predicates are required to be disjoint.
\end{definition}

Next, we provide the definition of FDR tasks.
FDR tasks are an extension of the $\text{SAS}^+$ planning formalism~\cite{helmert2009concise}.
They represent the type of task that is accepted by the search component of Fast Downward, and therefore one of the types the minimizer accepts for transformations.
Later, we refer to FDR tasks simply by $\text{SAS}^+$ tasks.

\begin{definition}[Planning tasks in finite-domain representation (FDR tasks)]
    A \textbf{planning task in finite-domain representation (FDR task)} is given by a 5-tuple $\Pi=\langle\mathcal{V},s_0,s_\star,\mathcal{A},\mathcal{O}\rangle$ with the following components:
    \begin{itemize}
        \item $\mathcal{V}$ is a finite set of \textbf{state variables}, where each variable $v\in\mathcal{V}$ has an associated finite domain $\mathcal{D}_v$.
        State variables are partitioned into \textbf{fluents} (affected by operators) and \textbf{derived variables} (computed by evaluating axioms).
        The domains of derived variables must contain the \textbf{default value} $\bot$.\\
        A \textbf{partial variable assignment} over $\mathcal{V}$ is a function $s$ on some subset of $\mathcal{V}$ such that $s(v)\in\mathcal{D}_v$ wherever $s(v)$ is defined.
        A partial variable assignment is called a \textbf{state} if it is defined for all fluents and none of the derived variables in $\mathcal{V}$.
        It is called an \textbf{extended state} if it is defined for all variables in $\mathcal{V}$.
        In the context of partial variable assignments, we write $v=d$ for the variable-value pairing $\langle v,d\rangle$ or $v\mapsto d$.
        \item $s_0$ is a state over $\mathcal{V}$ called the \textbf{initial state}.
        \item $s_\star$ is a partial variable assignment over $\mathcal{V}$ called the \textbf{goal}.
        \item $\mathcal{A}$ is a finite set of (FDR) \textbf{axioms} over $\mathcal{V}$.
        Axioms are triples $\langle\text{cond},v,d\rangle$, where $\text{cond}$ is a partial variable assignment called the \textbf{condition} or \textbf{body} of the axiom, $v$ is a derived variable called the \textbf{affected variable}, and $d\in\mathcal{D}_v$ is called the \textbf{derived value} for $v$.
        The pair $\langle v,d\rangle$ is called the \textbf{head} of the axiom.\\
        The axiom set $\mathcal{A}$ is partitioned into a totally ordered set of \textbf{axiom layers} $\mathcal{A}_1\prec\cdots\prec\mathcal{A}_k$ such that within the same layer, each affected variable must appear with a unique value in all axiom heads and bodies.
        In other words, within the same layer, axioms with the same affected variable but different derived values are forbidden, and if a variable appears in an axiom head, then it may not appear with a different value in a body.
        This is called the \textbf{layering property}.
        \item $\mathcal{O}$ is a finite set of (FDR) \textbf{operators} over $\mathcal{V}$.
        An operator $\langle\text{pre},\text{eff}\rangle$ consists of a partial variable assignment $\text{pre}$ over $\mathcal{V}$ called its \textbf{precondition}, and a finite set of \textbf{effects} $\text{eff}$.
        Effects are triples $\langle\text{cond},v,d\rangle$, where $\text{cond}$ is a (possibly empty) partial variable assignment called the \textbf{effect condition}, $v$ is a fluent called the \textbf{affected variable}, and $d\in\mathcal{D}_v$ is called the \textbf{new value} for v.
    \end{itemize}
    For axioms and effects, we commonly write $\text{cond}\rightarrow v:=d$ in place of $\langle\text{cond},v,d\rangle$.

\end{definition}


\section{Hill-Climbing}\label{sec:hill-climbing}
The goal of our implemented minimizer is to find a smaller task than the initial one that still fulfills some given property.
We as users of the minimizer are mainly interested in the resulting task and not in the steps taken to reach it.
When the path to a solution is irrelevant, local search algorithms can be a valid choice.
In local search, only the current search node is stored and its neighborhood is considered in the search for better candidates.
The simplicity and memory-efficiency of local search come at a price: (1) The algorithm might not find a solution, even though one exists, i.e., it is not \textit{complete} and (2) a solution is found and the algorithm terminates, despite a better solution being available, i.e., it is not \textit{optimal}.\\
\emph{Hill-climbing} is considered the simplest variant of local search.
In hill-climbing, the search process is initiated with an initial state.
Its neighborhood is then evaluated by assigning each neighboring state a value, e.g., the estimated goal distance via a heuristic function.
The most promising neighbor becomes the new search candidate and, again, its neighbors are considered.
The algorithm terminates when no neighbor's evaluation is better than the current candidate's.
This may of course lead to local optima.
Algorithm~\ref{alg:hillclimbing} describes hill-climbing in pseudo-code:

\begin{algorithm}[H]
    \caption{Hill-Climbing}
    \label{alg:hillclimbing}
    \begin{algorithmic}[1] % The number tells where the line numbering should start
        \State $\mathit{current}\gets\text{initial candidate}$
        \Loop
            \State $\mathit{next}\gets\text{a neighbor of \emph{current} with maximal value}$
            \If{$\text{value of }\mathit{next}\leq\text{value of }\mathit{current}$}
                \State \Return{\textit{current}}
            \EndIf
            \State $\mathit{current}\gets\mathit{next}$
        \EndLoop
    \end{algorithmic}
\end{algorithm}

The hill-climbing algorithm we just described relies on a way to rank neighboring states by their values and chooses the most promising one.
When the number of neighbors becomes very large, it might become infeasible to generate and store all of them, let alone rank them according to a heuristic or objective function.
For those reasons, \textit{first-choice hill-climbing} was used in the task minimizer implemented for this project.
This variant of hill-climbing generates neighbors of the current task and picks the first higher-valued neighbor encountered as the new \textit{current}.
In \cref{sec:task-transformations} of \cref{ch:impl}, we describe how we use this algorithm.


