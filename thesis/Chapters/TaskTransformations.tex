\chapter{Task Transformations}\label{ch:task-transformations}

The implemented task minimizer offers three main options to shrink a given PDDL task: deleting actions, deleting predicate symbols and deleting objects.
For $\text{SAS}^+$ tasks, the two implemented options are the deletion of operators and the deletion of variables.
The deletion of actions in PDDL tasks is not very complex, as the action chosen to be deleted simply can be removed from the set of actions of the task.
The same holds for operators in $\text{SAS}^+$ tasks.
On the other hand, to correctly delete a predicate symbol, object or variable from a task, we need a more sophisticated approach.
For example, if an object with the name ``box'' should be removed from a PDDL task, it is not sufficient to remove it from the set of objects of the task.
If ``box'' still occurs in the initial state or the goal, this can lead to incorrect behavior of a planner.
In the following sections, we define how the minimizer deletes different task elements from PDDL and $\text{SAS}^+$ tasks.
It is important to note that task transformations performed by the minimizer do not preserve task semantics.
The goal of task transformations in the scope of this thesis project is to obtain a smaller task through deletion of one of the described task elements.

\section{Deletion of Actions}\label{sec:deletion-of-actions}
Since actions are not part of other task elements, their deletion is a simple process.
\begin{definition}[Deletion of Actions]
    The deletion of action $\alpha$ from a given PDDL task entails the removal of $\alpha$ from the set of actions of the task.
\end{definition}

\section{Deletion of Predicates}\label{sec:deletion-of-predicates}
Wherever a predicate symbol $P$ is a part of a greater formula, it does not make sense to simply delete it.
In that case, the atom or literal containing $P$ needs to be replaced with $\top$ (\textit{truth}) or $\bot$ (\textit{falsity}).
By default, the minimizer replaces a literal containing $P$ with $\top$.
This corresponds to a projection~\cite{edelkamp2001planning} on all predicate symbols but $P$, since the parsed task is in negation normal form~\cite{helmert2009concise}.
%, which can be specified when the program is executed.
When running the minimizer, there are also the options of setting all atoms containing $P$ to either $\top$ or $\bot$.
After such a replacement, a method from the Fast Downward translator called \texttt{simplified} is called on the entire formula the replaced literal or atom was a part of, which gets rid of redundancies such as $\varphi\vee\top\equiv\top$ or $\varphi\wedge\top\equiv\varphi$ for formulas $\varphi$.
The following definition is for the default case, where literals containing the predicate symbol to be deleted are replaced with $\top$.
The definitions for the two other options are very similar, except that, instead of literals being replaced with $\top$, atoms are replaced with $\top$ or $\bot$, respectively.
\begin{definition}[Deletion of Predicate Symbols]
    The deletion of predicate symbol $P$ from a given PDDL task entails the following operations:
    \begin{itemize}
        \item $P$ is removed from the set of predicates of the task.
        \item Each atom in the initial state of the task containing $P$ is removed from the initial state.
        \item Each literal containing $P$ in the goal formula is replaced with $\top$.
        \item For each action, the following operations are performed:
        \begin{itemize}
            \item Each literal containing $P$ in the action precondition formula is replaced with $\top$.
            \item If an action's precondition becomes $\bot$, the entire action is deleted.
            \item Each literal containing $P$ in the PDDL effect is replaced with $\top$.
            \item If the PDDL effect is conditional, each literal containing $P$ in the effect condition is replaced with $\top$.
            \item If the PDDL effect is conditional and its condition becomes $\bot$, the entire conditional effect becomes $\bot$.
            \item If the entire PDDL effect becomes $\top$ or $\bot$, the entire action is deleted.
        \end{itemize}
        \item For each axiom, the following operations are performed:
        \begin{itemize}
            \item If $P$ occurs in the head of the axiom, the entire axiom is deleted.
            \item Each literal containing $P$ in the body of the axiom is replaced with $\top$.
            \item If the body of the axiom becomes $\bot$, the entire axiom is deleted.
            \item If the body of the axiom becomes $\top$, an artificial dummy predicate with arity 0 is created to become the new axiom body and act as a trigger for the axiom.
            An atom with this new predicate is also added to the initial state of the task.
        \end{itemize}
    \end{itemize}

\end{definition}

\section{Deletion of Objects}\label{sec:deletion-of-objects}
The deletion of objects is simpler than the deletion of predicates in terms of complexity.
Atoms in the goal formula containing the object to be deleted are replaced with $\top$.
\begin{definition}[Deletion of Objects]
    The deletion of object $\omega$ from a given PDDL task entails the following operations:
    \begin{itemize}
        \item $\omega$ is removed from the set of objects of the task.
        \item Each atom in the initial state of the task containing $\omega$ is removed from the initial state.
        \item Each action containing $\omega$ is deleted.
        \item Each atom in the goal of the task that contains $\omega$ is replaced with $\top$.
    \end{itemize}
\end{definition}

\section{Deletion of Operators}\label{sec:deletion-of-operators}
The deletion of operators from $\text{SAS}^+$ tasks can be considered identical to the deletion of PDDL actions.
\begin{definition}[Deletion of Operators]
    The deletion of operator $o$ from a given $\text{SAS}^+$ task entails the removal of $o$ from the set of operators of the task.
\end{definition}

\section{Deletion of Variables}\label{sec:deletion-of-variables}
In the implementation, variables are identified via an index.
Thus, in each transformation, when a variable with index $i$ is deleted, we decrease the indices of all variables with index $>i$ in the entire task by 1.
\begin{definition}[Deletion of Variables]
    The deletion of variable $v$ from a given $\text{SAS}^+$ task entails the following operations:
    \begin{itemize}
        \item $v$ is removed from the set of state variables of the task.
        \item For each mutex group, each fact containing $v$ is removed.
        \item $v$ is removed from the initial state.
        \item If a fact in the goal contains $v$, this fact is removed from the goal.
        \item For each operator, the following operations are performed:
        \begin{itemize}
            \item If a fact in the prevail condition contains $v$, it is removed from the prevail condition.
            \item If $v$ is the affected variable in an effect, this effect is removed from the operator.
            \item If a fact in an effect precondition contains $v$, it is removed from the effect precondition.
            \item If no effect remains in the operator, the entire operator is deleted.
        \end{itemize}
        \item For each axiom, the following operations are performed:
        \begin{itemize}
            \item If $v$ is the affected variable of the axiom, the axiom is deleted.
            \item If a fact in the body of the axiom contains $v$, it is removed from the axiom body.
        \end{itemize}

    \end{itemize}
\end{definition}